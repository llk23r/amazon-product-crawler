![MetaFetcha](amazon_scraper/static/Home.png)

### Description:
A Scrapy-Flask application that fetches product details including the reviews [currently
limited to first 3 pages only] from Amazon for the provided url.
It also fetches some meta from Flipkart (_Refer Changelog v1_).

<hr>

### Changelog v1:
- The app now fetches the following attributes from Flipkart if the product is
  available there.

    - Flipkart url
    - Rating on Flipkart
    - Price on Flipkart
    - Images on Flipkart [if available]

- Updated README
<hr>

### Requirements:
- Flask
- Postgresql
- Scrapy
- A valid url of the product.

### Instructions to run it locally:
- Install all relevant packages using `pipenv install`.
- Then activate the virtual environment using `pipenv shell`.
- Create a db called `meta_fetcha`
- Then run the migrations using `alembic upgrade head`.
- Once the migrations are run, run the `flask server` at port 5000 using
  `python app.py`
- Once the server starts, navigate to `localhost:5000` on your browser

    OR

  Use `POST` `localhost:5000/crawl` using a REST client with a JSON body
  containing the product url. Example:
  ```
  {
		"url": "https://www.amazon.in/Audio-Technica-ATH-M50x-Over-Ear-Professional-Headphones/dp/B00HVLUR86/"
  }
  ```
  Sample cURL:
  ```
  curl --request POST \
    --url http://localhost:5000/crawl \
    --header 'content-type: application/json' \
    --data '{
  	"url": "https://www.amazon.in/Audio-Technica-ATH-M50x-Over-Ear-Professional-Headphones/dp/B00HVLUR86/"
  }'
  ```
- After the crawl is over, you get back a JSON of the crawled product with the
  metadata.

### Sample Response:
```
[
  {
    "https://www.amazon.in/Audio-Technica-ATH-M50x-Over-Ear-Professional-Headphones/product-reviews/B00HVLUR86": {
      "categories": [
        "Electronics"
      ],
      "meta_data": {
        "amazon_availability": "In stock.",
        "amazon_price": "11200.0",
        "amazon_rating": "4.5",
        "features": "Critically acclaimed sonic performance praised by top audio engineers and pro audio reviewers Proprietary 45mm large-aperture drivers with rare earth magnets and copper-clad aluminum wire voice coils Exceptional clarity throughout an extended frequency range with deep and accurate bass response Circumaural design contours around the ears for excellent sound isolation in loud environments 90 degree swiveling earcups for easy, one-ear monitoring, professional-grade earpad and headband material delivers more durability and comfort Detachable cable",
        "flipkart_price": "₹12,990",
        "flipkart_rating": "4.7",
        "flipkart_url": "https://www.flipkart.com/audio-technica-ath-m50x-wired-headset-without-mic/p/itm4eeba4fce9226",
        "image_0": "https://rukminim1.flixcart.com/image/200/200/headphone/u/x/m/audio-technica-ath-m50x-400x400-imaduvfkzbabhfhg.jpeg?q=90",
        "image_1": "https://rukminim1.flixcart.com/image/200/200/headphone/u/x/m/audio-technica-ath-m50x-400x400-imaebewayqedq6dh.jpeg?q=90",
        "image_2": "https://rukminim1.flixcart.com/image/200/200/headphone/u/x/m/audio-technica-ath-m50x-400x400-imaebew4fjwz7zw4.jpeg?q=90",
        "price": "11200.0",
        "product_description": " ",
        "rating": "4.5"
      },
      "name": "Audio-Technica ATH-M50x Over-Ear Professional Studio Monitor Headphones (Black)",
      "reviews": [
        {
          "author_name": "APOORV PANSE",
          "author_path": "/gp/profile/amzn1.account.AEKNOOGWGB27FIIGSNKPIU6EO7CA",
          "review": "Edit : Updating the rating from 4 to 5 stars, after 10 months of usage. As the title says, I totally fell in love with it.________________________________________________________________________________________________________This review is for those, who are confused regarding the hype of M50X. I personally did not like the sound signature earlier. But after a lot of tweaking with equalizer and with enabling spatial sound (Dolby Atmos for windows 10), I am slowly falling in love with these headphones. It requires time to get used to the signature of sound if you are hardcore bass lovers.So here is my full review which I would like to break down by points :1) SOUND Quality:BASS 9/10: Excellent production of bass. After increasing bass levels in Equalizer, it just pumps right amount of bass and is very deep and punchy.MIDS 7 /10: Mids are okay, balanced. Vocals are crisp and bass does not overpower the mids.HIGHS 5 / 10: Awful. It's very bright. I had to lower down some higher frequencies in order to resolve my headache.I come from Sennheiser background where I had PX 80, PX 95, CX 175, HD 202, HD 203 and HD598 SR. I also have Sony MDR 550AP. So comparing to those, sound signature of ATH is different. If you directly plug in these headphones without configuring and tweaking equalizer as per your needs, then you would be disappointed, as I was on the first day of listening to these.I thought, maybe, I should try configuring properly. I googled frequency response and balanced equalizer as per my needs. I have uploaded my EQ settings. So below review is purely depending on my personalized EQ setting.Now, these headphone sound damn amazing, I could not believe the experience after enabling Dolby Atmos for Headphones (Windows 10 feature). It's brilliant. Even it sounds great on Windows Sonic spatial configuration. Just disable 7.1 virtual surround. Uncheck the box in sound setting. Games and movies sounds epic. High quality audio files have new dimension to it now. Especially A.R Rahman songs (Dil se and Jiya Jale recording in Berkeley University). If you love bass, these will definitely sound better than HD598. But if you love immersion and wider soundstage, no one can beat Sennheiser HD598 under 10k budget.However sound quality is very subjective, as many people prefer high bass, many prefer higher clarity. These headphones can be \"ADJUSTED\" according to your needs. Hence I can say, out of the box its not good, but it definitely has that capability to be the best if you give it proper attention.2) BUILD QUALITY 10/10 : Simply excellent. It feels premium. Very strong. It looks durable, many people have been using these for years. Although not sure about ear cups, as they wear out in one year or so.3) COMFORT 9/10 (Edited) : When I posted this review, I found it very difficult to wear these for long hours, but after 10 months of usage, the comfort level is far better now. I have used it for watching back to back movies, playing games for 4-6 hours straight without any issue. Although in warm weather, it may get a bit sticky. No issues in air conditioned room.4) NOISE CANCELLATION 6/10: Not that good. I can hear ambient noise. It reduces up to 50 percent. Best to listen in quiet environment.5) SOUND LEAK 10/10: Sound does not leak at all. Person sitting next to you will not be disturbed at all, even on high volume.6) Accessories 10/10 : Good carrying pouch, 3 wires as per different needs.",
          "review_rating": 5.0,
          "review_title": "Give it time, and you will slowly fall in love.",
          "verified_purchase": true
        },
        {
          "author_name": "Shravan",
          "author_path": "/gp/profile/amzn1.account.AFHHKWDAJINNG2AG4F2LUGYFCFHA",
          "review": "I bought this headphone in May 2017. It has been over a year since. This headphone is easily, EASILY, my best and most favorite electronics purchase of the decade. As someone who is passionate about music- both listening and creating, this headphone has been able to handle every possible scenario I have thrown at it.If you are the average music listener who listens to music while relaxing at home and wants the best sound quality at a great price, I would highly recommend you get yourself a pair (or two) of these headphones. Unless you have experienced more expensive, higher quality studio headphones, I will guarantee your mind will be blown.The frequency response on this headphone is not completely flat. They do pump up the bass a teeny little bit. The ATH-M40X is flatter than this. However, one must note that that a flatter sound does not necessarily mean a better experience. You could say that this headphone has a more \"colorful\" sound as compared to a flatter headphone. This makes listening to music at leisure an absolute delight as makes music \"feel\" good.That being said, this is also an excellent headphones for use at the studio. You will find a number of pro musicians on YouTube recording with this headphone (unfortunately Amazon review does not allow me to post YouTube links). They are that good!I listen to a lot of rock music, stuff like System Of A Down, Pink Floyd, Alt-J, etc. This headphone showed me things in music I did not know about! Did you know there is a piano in Chop Suey? I did not! Until I heard it with this!Besides sound, this headphone is super durable. I am not the most careful person with my things, I often throw stuff around. And this headphone has been through the depths of hell. I take them to my workplace every day; which means they bump around in my backpack along with a lot of other stuff. I wear them for 3-4 hours straight- super comfortable. My friends borrow my headphone, and who knows what they do with it. But it has survived! And without as much as a scratch!My only, ONLY complaint about this headphone was the lack of Bluetooth connectivity. Having a 3m cable hanging around isn't exactly the most convenient situation. The solution? I got . It costs a lot on Amazon India, but just 50 USD in Amazon US (a little less than 3,500 INR). I was able to source if from a friend there and now I have an excellent pair of BLUETOOTH headphones for roughly 13,000 INR!If these headphones do break eventually, you can bet that I will buy another pair without batting an eyelid. These things are worth so much more than their price. I cannot recommend them enough.",
          "review_rating": 5.0,
          "review_title": "I have found the headphone I will keep buying for the rest of my life.",
          "verified_purchase": true
        },
        {
          "author_name": "Subu",
          "author_path": "/gp/profile/amzn1.account.AHVY2B2WRRXJZEF4LCH422MAAODA",
          "review": "It’s for a while I am writing this review. After using for years, though of sharing my experience. This headphone is awesome. The big thing is the quality of headphone and accessories. There is actually not much of difference between bose, beats and audio technica unless you are professional in music industry. This headphone delivers what the specs say. You can spend 10k to 20k more to get a bose or beats but this guy does the job perfectly well. I love this headphone very much. No issues or complaints throughout these years. Highly recommend for music lovers.",
          "review_rating": 5.0,
          "review_title": "Excellent headphones",
          "verified_purchase": true
        },
        {
          "author_name": "Radhika",
          "author_path": "/gp/profile/amzn1.account.AGGIPI63EJ5QYFE2CBVABOX62A2A",
          "review": "These headphones are probably the most popular headphones at this price.They're advertised as \"Studio monitor headphones\". This means they shouldn't have overpowering bass, yet the bass is prominent. If you're buying to listen to music and actually ENJOY it, then definitely don't buy these. These headphones are just generally bad. They're not neutral, yet the bass that's there is annoying.The pads aren't comfortable at all, and wearing them for more than 10 minutes is ear fatigue hell. They don't completely cover the entire ear and rest on the outer area. This causes pain.Shame I can't return these, I'd love to.Edit: I bought the Beyerdynamic Custom Pro Plus (bass heavy) and they sound fantastic.",
          "review_rating": 2.0,
          "review_title": "Don't buy these, please.",
          "verified_purchase": true
        },
        {
          "author_name": "Hrithik Rawat",
          "author_path": "/gp/profile/amzn1.account.AHEVE6Z45L6UDGPDJGQLZLL3QCHQ",
          "review": "Pros:-The output is good for monitoring and basic mixing.-Comes with 3 different types of cables for different purposes.-Cable is detachable and hence can be changed if problematic.-Portable. It has swivel ear cups and also folds into half its size for travel portability.-Comes with a carry pouchCons:-Causes listening fatigue, isn't super comfortable for long listening hours-The output is a little coloured-They don't completely cover your ears.Note:I have had mine for the past 3years and the left output of the cup has become noticeably less.The cups have also become weary.I think as of 2019, there are better options available now.",
          "review_rating": 3.0,
          "review_title": "GOOD and BAD",
          "verified_purchase": true
        },
        {
          "author_name": "Jasmine",
          "author_path": "/gp/profile/amzn1.account.AFKIG6GL5G52QJ2NH4TGQHGLWG7Q",
          "review": "With very strong words i woould like to tell you this is strictly audiophile studio headphone where people use it for monitoring purpuse .. so they use ir for 4 hr max ... Just to monitor not to enjoy music .. if you looking dor music headphone this is not the one .kindly read all 3 - 1 stars comments alla are true .. its totally un comfortable to wear it .. it treable sound so sharp u will feel headache after 10 -15 mins .... Ears will get warm .. sound is really clear in details ..but surely not to enjoy music... Better go for sony Sennheiser bose at this range ... This is strictly usage for monitoring sound .... I would say if you looking this for music listening just run away.. dont list any good review they are true at same time all other rating is also true ... 3-1 rating are so true that u will only understand after buying it.",
          "review_rating": 3.0,
          "review_title": "Dont buy this headphone its strict for monitoring",
          "verified_purchase": true
        },
        {
          "author_name": "Amazon Customer",
          "author_path": "/gp/profile/amzn1.account.AGN6ZRROAQNQZ5DC7CI6XNJDNPVA",
          "review": "Until this pair of head phones i have been using a pair from Jabra called the revo wireless that have lasted me a long time.this pair has the best quality of sound that i have ever seen in a headphone.if you are willing to spend the money i think these will make a good pair of headphone. built quality is good but not as good as the jabrahope these will last as long as those ones.My purpose was to play games and listen to music primarily.",
          "review_rating": 5.0,
          "review_title": "go for them if you are willing to spend the amount. worth the price.",
          "verified_purchase": true
        },
        {
          "author_name": "K. S",
          "author_path": "/gp/profile/amzn1.account.AF3O4FJNOWPFZ7CQIRYATYYA5AFQ",
          "review": "Enhanced Low and high frequencies. Suitable for casual listening and not ideal for critical listening.Pros:* More emphasis on bass and highs are punchy but without bleeding too much into other frequencies unlike other cheap bass enhanced headphones.* Feels strong and comes in multiple cables of different sizes.* Suitable for casual listening* Decent sound isolation.* Can be driven by less power sources like mobile, portable music player, etc. But would need a good source (I meant some device with good DAC)Cons:* Horrible, horrible mids. AKG k92 which is less than half the price of this is way clear and accurate* Poor imaging , instrument separation and soundstaging. AKG k92 way way better in this regard.* After an year of use, the headphone starts making annoying cracking sound even with the slightest of head movement. It has reduced after applying lubricating oil in all of the hindges* Comfort: not the best at this price range.* Listener fatigue & HarshUPDATE:After getting used to true entry-level audiophile / professional headphones like AKG K712, K92, etc; This headphone doesnt' standup to the hype! Calling ATH-M50x a studio headphone is not justifiable; given that we have other good studio grade headpones in the same price range from Beyerdynamic, AKG to name a few.This definitely has better sound than mass market headphones (Beats,..). But paying 10ks for this pair of cans doesn't do any justice! You might like them if you listen to Pops, EDMs, high beats music, hip hop, rap, rock and heavy metal. But for classical and songs with good mids and vocals, this is not recommended.Music quality is subjective. But this is cannot be an entry-level audiophile grade or studio headphone. It may feel better if you have never listed to good neutral headphones. Listen to Beyerdynamic or AKG headphones in the same range OR sony mdr-v6 to feel the difference.",
          "review_rating": 3.0,
          "review_title": "Punchy bass and piercing highs. Horrible mids and vocals. Good for casual listening",
          "verified_purchase": true
        },
        {
          "author_name": "Vijay B",
          "author_path": "/gp/profile/amzn1.account.AEJE2DS7GVIBZIYC53WBTIC2FMGA",
          "review": "First up, the sound quality is unreal. It's so damn good for video editing and even casual listening. I havent really used it with an amp yet but it's still great. But:1. It's very very VERY uncomfortable. These headphones are OK in air conditioned rooms. Even if the AC goes for about 5 minutes you ll start feeling the heat. The clamp force is terrible inspite of my best efforts to widen it by leaving it on hard surfaces all night.2. Build is... Average - the headphones feel just great is hand but the ear cushion started flaking in 6 months. Same with bottom of the headband. This inspite of me keeping it in bag as much as I could.3. Terrible for outdoor or relaxed use - if you want to use it normally on your bed or while travelling on public transport it sweats like crazy. Maybe a reason for the flaking. It's a bad headphone for casual listening so don't buy it if you want to do that.",
          "review_rating": 4.0,
          "review_title": "Good sound quality. But Headphone s cushion and band age horribly.",
          "verified_purchase": true
        },
        {
          "author_name": "Viv",
          "author_path": "/gp/profile/amzn1.account.AFKC4MIX2FLOIMUVOTEAFNVVT5GQ",
          "review": "Honestly curiosity got the better of me with this one. The MOST talked about headphones on the Internet whether you love em or hate em. So I had to get one. And I love the customisation options including skins and wraps for A-T headphones. Takes me back to my college days that were a long long time ago.If you like bright sound with strong punchy bass and are looking to upgrade to good quality headphones this is the one to get. TBH these are not as flat as claimed to be but have a distinct V shaped FR curve.There's nothing much I can add to what has not been already said here and elsewhere on the 'net. But if you are looking at a great full size headphone upgrade from a crappy set of in-ears included with your mobile phones this is the one to get. Best of all it comes with 3 different cables for you kind of use. I find the 3 meter coiled cable best for me.Sound - 4 stars, bright, revealing punchy and at ear piercing. Coming from the world of Sennheisers audiophile grade headphones I felt someone was poking a needle in my ear drum. I winced and gave them time to run in and open up which they did. These are studio monitors after all. They sound good with rock, pop and EDM. Not so much with Jazz and instrumental/classical. Not a set of cans I will use for day long listening because they cause ear fatigue. Sound stage is limited. But to be fair these cans aren't meant for home use although that is precisely what most owners use them for.Construction - 4.5 stars. Plastic with a steel headband. They look hardy and should last a long time even if given a few knocks. Well constructed like other M series pro monitors.Comfort - 2 stars, I have medium-large ears and they squeeze into ear pads but only just. The ear lobes are pinched and cause pain. Replacing the ear pads with after market pads affects the sound in varying degrees. So tread carefully here. I bought Brainwavz memory foam pads just in case.All in all a good start and introduction to the world of A-T pro monitor headphones. And now this has a place in my collection.But I can at best give them 4 stars overall and I am being generous here. There are better choices in the world of A-T headphones. If you can pony up about 50% more, look at the M50xBT for a similar sound signature with smoother sound, better comfort and the convenience of wireless operation. Or best of all, get the M40x for more at a marginally lower price, comparatively balanced sound but with a compromise on comfort.One word of caution - A-T's customer support is non existent in India. I emailed them via their site enquiring about ear pads and have not received a response. Not an encouraging sign IMO.",
          "review_rating": 4.0,
          "review_title": "4 stars - high quality headphones, an ideal upgrade from tinny in-ear monitors",
          "verified_purchase": true
        },
        {
          "author_name": "Allcan Canicious",
          "author_path": "/gp/profile/amzn1.account.AH6XECVG5QARD35ERIQMEYSL7MPQ",
          "review": "After 3 days of extensive listening... I've come to conclusion that these perform well with all genres of songs.Don't get carried away with all the reviews which mention bass is extraordinary, bass and low end performance is present but not as much as the sony xtra bass series, when a song or an audio has a particular bass it is reflected perfectly and may be slight emphasised.I personally am a treble freak and the treble spark is amazing on these headphones, im hearing new unrecognized music nuances from my favourite tracks.Will post a review after few months of usage, since most people talk about better audio performace after burn in.",
          "review_rating": 5.0,
          "review_title": "Not only bass but treble lovers will also be completely satisfied",
          "verified_purchase": true
        },
        {
          "author_name": "Deepak Dhamuria",
          "author_path": "/gp/profile/amzn1.account.AFUQHT7YS5XIKTQ776MDD3XXFHJA",
          "review": "For me a good headphone doesn't just mean deep bass as most people want. I want every single instrument and vocal to be heard properly on any Equalizer setting. This headphone exceeded my expectations. The sound is very very clear and crisp. The best use of this headphone comes when you are listening to FLAC files. It just gives you the best experience.",
          "review_rating": 5.0,
          "review_title": "Best headphones I have purchased till date",
          "verified_purchase": true
        },
        {
          "author_name": "VS",
          "author_path": "/gp/profile/amzn1.account.AE23FEQMCOUDAHYIOHG5MMNBMFRQ",
          "review": "I received the headphones a month back but delayed writing this review because some reviews said that they sound better after they have been used for a while. Straight out of the box the bass boomed and I even thought of returning it. However, as reported in the reviews the sound improved with use and now, after a month, I have one of the best sounding headphones that I have ever used. It is not only value for money but also one that can hold its own against much more expensive headphones.",
          "review_rating": 5.0,
          "review_title": "... this review because some reviews said that they sound better after they have been used for a while",
          "verified_purchase": true
        },
        {
          "author_name": "nich raja",
          "author_path": "/gp/profile/amzn1.account.AEQ4Z6EKBN66N4DKDP6YTZN6DXKA",
          "review": "God of all gaming headset. After a month research on YouTube, google etc I finally made my choice for Ath mx50 , keeping aside with heavy heart the brands like hyperx, logitech series etc. This device delivers precise n actual position of enemy in pubg. No need to look at maps for footsteps. I was at crown 2 during most of season but now I reached Ace tier with 2.17 kd in 450 matches as on 08/06/20. Guinune review from Arunachal pradesh.",
          "review_rating": 5.0,
          "review_title": "Now or never. Grab the opportunity.",
          "verified_purchase": true
        },
        {
          "author_name": "Faizan Hussain",
          "author_path": "/gp/profile/amzn1.account.AEURTE2FQPUCRSUCEGXXSKSO23SQ",
          "review": "It has not even been a month and the right ear of the headphone does not have the same volume as the left. I wonder why!! I had heard great reviews about these headphones but I am not that happy after using it. Wish they had a slightly longer window for replacement. Very disappointed. Not sure if it is Amazon who has sent a faulty piece or Auto-Technica!!! Please help if any of the two are reading. Thanks.",
          "review_rating": 1.0,
          "review_title": "Right ear’s volume is less than the left!!",
          "verified_purchase": true
        },
        {
          "author_name": "Sandipan T.",
          "author_path": "/gp/profile/amzn1.account.AGJGQ72KCLLMGTES7DNWWOOETMLQ",
          "review": "This product has been an eye opener for me. I don't produce or work with music, and after having used this I was exposed to an entire new spectrum of sound. All this time, I was trying get the best earphones/headphones that can reproduce the best sound. But after using these headphones, I realized that the sound quality and source is equally important. I have been using it with a Fiio A3BK amplifier and I can make out the slightest difference in the sound quality or the level of noise. This is NOT a commercial headphone, so I had to work and tune a lot to get the kind of sound I like.If you just want to plug a headphone to a music source and listen, these are NOT for you. You'll have to work hard for it, spend time tuning it to exactly what you want.P.S Please don't forget to pair it with a good DAC & AMP for the perfect crystal clear studio like sound coz that is what this thing is meant to do.P.S.S I have been owning these for more than a year now. I just bought a premium flagship phone which has really good DAC. And I am pleased to say that I am now getting the mileage that I was looking for. The amplifier makes the already good sound blossom and these headphones does a perfect job of reproducing them.In my limited experience I'd say that it is close to that studio quality sound I was looking for.",
          "review_rating": 5.0,
          "review_title": "I was trying get the best earphones/headphones that can reproduce the best sound",
          "verified_purchase": true
        },
        {
          "author_name": "Abhijit A Misra",
          "author_path": "/gp/profile/amzn1.account.AGP55YXHW23AAJ7XLUX54RYG6C6Q",
          "review": "I purchased these headphones nearly 3 years ago, and i must say these have truly exceeded my expectations in almost every way possible... I daily use them for approximately 4-5 hours at a stretch, sometimes even more but every time i use them they simply amaze me with their sound quality and deepness, it's like when you are watching a movie in which two persons are conversing with each other, but there's also a subtle whistling sound or a sound of a breeze in the background which you never knew existed until you used these headphones, or like when you are listening to a fine guitar based karaoke and you get to listen the subtlest and minutest part of the music, this is the quality that you truly get from these headphones!!.. Secondly when it comes to handling, i am certainly not a pro in it and i have used them roughly sometimes but amazingly they never got broke or met with any kind of problem, including earpads, they have got a little worn out as i use my headphones too much, but still are working fine(touchwood)... sound quality is still the same as day 1!!... After all, why does an average user purchases a headphone, obviously to listen to his favourite song, watch a movie, listen to audiobooks, podcasts, etc... in that case one certainly desires a premium quality headphone that delivers perfectly, that allows you to immerse in your listening experience and forget the world for sometime, if you are that guy these headphones certainly fits your bill.... In a tough evolving market where every item you purchase becomes outdated overnight, these headphones have fared exceptionally well, even among the critics... they have not earned their reputation by false testimonials!!.. So believe the hype, because it's true...",
          "review_rating": 5.0,
          "review_title": "It truly lives up to it's hype",
          "verified_purchase": true
        },
        {
          "author_name": "😎",
          "author_path": "/gp/profile/amzn1.account.AEXHQOAGLFAT7HBPVHT6QRC7DNYQ",
          "review": "really amazing crisp soundBass is something that I really appreciate unlike every other normal pair of headphones bass is kinda balanced, since this thing is for studio monitoring it's really useful and the accessories & build quality are worthy of every penny spent. side note : nothing beats this in this price rangeClear vocalsInstrument separationNo damping of effects like in marshall major 2It's all bcz audiotechnica doesn't show off unlike other brands like 'beats', Bose, Sony etc cuz this company invests greatly on the product rather on advertising it that much like beats and others doPlease buy if u really searching for studio monitoring these are built for that purposeI won't say only that purpose cuz they sound perfectly fine for casual music listeningCons90° swivel is kinda annoying it is used for one ear monitoring they said but I don't use it that much so it's annoyingOnly it's design is a bit notchyAnd the size of ear pads is little bit small to people with big ear pinnaeBottom line : GO BUY IT IF U WANT",
          "review_rating": 5.0,
          "review_title": "Incredible 😍😍😍😍",
          "verified_purchase": true
        },
        {
          "author_name": "Avik Samaddar",
          "author_path": "/gp/profile/amzn1.account.AHAADWSQVMYXCBIQJ2B5LTIH2FEQ",
          "review": "Listening on these headphone is like nirvana... Its like music playing right in your head. I have a audio technica ath m30x which itself is phenomenal and can blow out of water any headphones way pricier than them but this thing makes m30x looks like a basic pair. If you are particular about your listening habits and can shed 10k....look no further as anything better than these will have a price tag worth your kidneys with practically no extra noticeable difference at all.... I am a classical singer and i can assure you that.",
          "review_rating": 5.0,
          "review_title": "You don't need a sennheiser hd800 to enjoy music anymore",
          "verified_purchase": true
        },
        {
          "author_name": "Venkatesh C",
          "author_path": "/gp/profile/amzn1.account.AHMVYGPPAGMCKAODIDKQ537ESCRQ",
          "review": "Fantastic sound! If you are used to budget and bass heavy headphones, the sound will take a little while to get used to. But then you'd start noticing new sounds in familiar tracks and start appreciating that this headphones tries to maintain a flat signature (Imagine your graphic equalizer set to flat.. ) and What you hear is what the music director tuned. This works very well with mobile phones and laptops and no amp is needed. The only downside is that after a year of standard use, your ear pads will start to flake and fall apart. I'd buy this anyday again!",
          "review_rating": 5.0,
          "review_title": "The best neutral headphone that sits bang between premium and budget categories",
          "verified_purchase": true
        }
      ]
    }
  }
]
```

### Caveats:
- Amazon tends to block the requests. So, if you receive a 503, you'll have to
  wait it out if you start receiving 503.

A quick way to test it out would be to run the following:
`scrapy shell <url to be scraped>`. This takes you to a shell where you can
inspect the requests and responses.

import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, subqueryload
import projectconfig
from models.sites import Site
from models.categories import Category
from models.products import Product
from models.metadata import Metadata
from models.reviews import Review
from models.product_meta import ProductMeta
from dateutil.parser import parse
from urllib.parse import urlparse
from services.flipkart_meta_fetcher_service import FlipkartMetaFetcherService

db_engine = create_engine(projectconfig.db_url, pool_size=80, max_overflow=0)
Session = sessionmaker(bind=db_engine)
session = Session()


class ModelResolverService:
    def __init__(self, item, response):
        self.response = response
        self.item = dict(item)
        self.prod_dict = self.item.get('product')
        self.prod_name = self.prod_dict.get('product_name')
        self.session = session
        self.reviews = self.item.get('reviews')

    def _resolve_site_url(self):
        parsed_uri = urlparse(self.response.url)
        result = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return result

    def create_and_resolve_site(self):
        site = self._resolve_site_url()
        existing_site = self.session.query(Site).filter_by(
            name=site, url=self.response.url, archived_at=None).order_by(Site.created_at.desc()).first()
        if existing_site:
            site_id = existing_site.id
        else:
            site = Site(name=site, url=self.response.url)
            self.session.add(site)
            self.session.flush()
            site_id = site.id
            self.session.commit()
        self.site_id = site_id
        return site_id

    def _resolve_categories(self):
        categories = self.item.get('categories')
        return categories.split(',') if categories else []

    def resolve_product_category_wise(self, categories):
        product_ids = []
        for category in categories:
            existing_category = self.session.query(Category).filter_by(name=category, site_id=self.site_id, archived_at=None).order_by(Category.created_at.desc()).first()
            if not existing_category:
                category_obj = Category(
                    name=category,
                    site_id=self.site_id
                )
                self.session.add(category_obj)
                self.session.flush()
                category_id = category_obj.id
            else:
                category_id = existing_category.id
            existing_product = self.session.query(Product).filter_by(name=self.prod_name, site_id=self.site_id, category_id=category_id, archived_at=None).order_by(Product.created_at.desc()).first()
            if not existing_product:
                self.existing_product = False
                product = Product(
                    name=self.item.get('product').get('product_name'),
                    site_id=self.site_id,
                    category_id=category_id
                )
                self.session.add(product)
                self.session.flush()
                product_id = product.id
            else:
                self.existing_product = True
                product_id = existing_product.id
            product_ids.append(product_id)
            return product_ids

    def resolve_product(self):
        product_ids = []
        existing_product = self.session.query(Product).filter_by(name=self.prod_name, site_id=self.site_id, category_id=None, archived_at=None).order_by(Product.created_at.desc()).first()
        if not existing_product:
            self.existing_product = False
            product = Product(
                name=self.prod_name,
                site_id=self.site_id
            )
            self.session.add(product)
            self.session.flush()
            product_id = product.id
        else:
            self.existing_product = True
            product_id = existing_product.id
        product_ids.append(product_id)
        return product_ids

    def create_and_resolve_products(self):
        categories = self._resolve_categories()
        if categories:
            product_ids = self.resolve_product_category_wise(categories)
        else:
            product_ids = self.resolve_product()
        session.commit()
        self.product_ids = product_ids
        return product_ids

    def create_product_wise_reviews(self):
        for product_id in self.product_ids:
            for review in self.reviews:
                verified_purchase = review.get('verifiedPurchase')
                review_rating = review.get('starRating')
                if review_rating and review_rating != '':
                    review_rating = float(review_rating[:3])
                else:
                    review_rating = 0.0
                review_date = review.get('postDate')
                author_name = review.get('name')
                review_title = review.get('reviewTitle')
                author_path = review.get('reviewerLink')
                review = review.get('reviewBody')
                review_date = parse(review_date.split(' on ')[-1])
                verified_purchase = True if verified_purchase != '' and verified_purchase else False
                if self.existing_product:
                    session.query(Review).filter_by(product_id=product_id, author_path=author_path).update({Review.verified_purchase: verified_purchase, Review.name: author_name, Review.review_title: review_title, Review.review: review, Review.review_date: review_date, Review.review_rating: review_rating})
                else:
                    review_obj = Review(
                        product_id=product_id,
                        name=author_name,
                        author_path=author_path,
                        review_title=review_title,
                        review=review,
                        verified_purchase=verified_purchase,
                        review_date=review_date,
                        review_rating=review_rating
                    )
                    session.add(review_obj)
                    session.flush()
        session.commit()

    def create_and_resolve_meta(self):
        amazon_meta_dict = {}
        flipkart_meta_dict = {}
        amazon_meta_dict['product_description'] = self.prod_dict.get('product_description')
        amazon_meta_dict['features'] = self.prod_dict.get('product_features')
        amazon_meta_dict['amazon_rating'] = self.prod_dict.get('product_rating')
        amazon_meta_dict['amazon_price'] = self.prod_dict.get('product_sale_price')
        amazon_meta_dict['amazon_availability'] = self.prod_dict.get('product_availability')
        fmfs = FlipkartMetaFetcherService(self.prod_name)
        flipkart_meta_dict = fmfs.page_search()
        meta_dict_combo = {**amazon_meta_dict, **flipkart_meta_dict}
        meta_ids = []
        for meta_key, meta_value in meta_dict_combo.items():
            meta_value = str(meta_value)
            if self.existing_product:
                meta_id = self.session.query(Metadata).filter_by(name=meta_key, value=meta_value, site_id=self.site_id).update({Metadata.name: meta_key, Metadata.value: meta_value, Metadata.site_id: self.site_id})
                if meta_id:
                    meta_ids.append(meta_id)

            if (self.existing_product and not meta_id) or not self.existing_product:
                meta_obj = Metadata(
                    name=meta_key,
                    value=meta_value,
                    site_id=self.site_id
                )
                self.session.add(meta_obj)
                self.session.flush()
                meta_ids.append(meta_obj.id)
        self.session.commit()

        for product_id in self.product_ids:
            for meta_id in meta_ids:
                existing_map = self.session.query(ProductMeta).filter_by(metadata_id=meta_id, product_id=product_id).all()
                if existing_map:
                    self.session.query(ProductMeta).filter_by(metadata_id=meta_id, product_id=product_id).update({ProductMeta.metadata_id: meta_id, ProductMeta.product_id: product_id, ProductMeta.active: True})
                else:
                    product_meta_obj = ProductMeta(
                        metadata_id=meta_id,
                        product_id=product_id,
                        active=False
                    )
                    self.session.add(product_meta_obj)
                    self.session.flush()
        self.session.commit()
        self.session.close()

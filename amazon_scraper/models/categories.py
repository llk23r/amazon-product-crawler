from sqlalchemy import Column, Integer, String, Boolean, Text, DateTime, JSON,\
    ForeignKey, func, Index, inspect, text, ARRAY
from sqlalchemy import event
from models.base import Base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property

from sqlalchemy.dialects.postgresql import UUID, JSONB
import json
import time


class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    site_id = Column(Integer, ForeignKey('sites.id'), nullable=False)
    archived_at = Column(DateTime)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, onupdate=func.now(), default=func.now())

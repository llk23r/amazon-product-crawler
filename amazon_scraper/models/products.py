from sqlalchemy import Column, Integer, String, Boolean, Text, DateTime, JSON,\
    ForeignKey, func, Index, inspect, text, ARRAY
from sqlalchemy import event
from models.base import Base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property

from sqlalchemy.dialects.postgresql import UUID, JSONB
import json
import time


class Product(Base):
    __tablename__ = 'products'

    id = Column(UUID(as_uuid=True), primary_key=True,
                server_default=text("uuid_generate_v4()"))
    name = Column(String)
    source_url = Column(Text)
    site_id = Column(Integer, ForeignKey('sites.id'))
    # description = Column(Text)
    # features = Column(Text)
    # rating = Column(Float)
    # price = Column(Float)
    # review_id = Column(UUID(as_uuid=True), ForeignKey('reviews.id'))
    reviews = relationship("Review", backref='products', lazy=True)
    categories = relationship("Category")
    category_id = Column(Integer, ForeignKey('categories.id'))
    sites = relationship("Site")
    site_id = Column(Integer, ForeignKey('sites.id'))
    archived_at = Column(DateTime)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, onupdate=func.now(), default=func.now())

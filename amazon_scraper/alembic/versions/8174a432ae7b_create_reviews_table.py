"""Create reviews table

Revision ID: 8174a432ae7b
Revises: 2d3ac455e294
Create Date: 2020-07-27 01:23:55.945563

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = '8174a432ae7b'
down_revision = '2d3ac455e294'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'reviews',
        sa.Column('id', UUID(as_uuid=True), primary_key=True),
        sa.Column('name', sa.String),
        sa.Column('product_id', UUID(as_uuid=True),
                  sa.ForeignKey('products.id')),
        sa.Column('author_path', sa.Text),
        sa.Column('review_title', sa.Text),
        sa.Column('review', sa.Text),
        sa.Column('verified_purchase', sa.Boolean),
        sa.Column('review_date', sa.DateTime),
        sa.Column('review_rating', sa.Float),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('reviews')

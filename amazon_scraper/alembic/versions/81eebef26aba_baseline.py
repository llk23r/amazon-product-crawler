"""baseline

Revision ID: 81eebef26aba
Revises: 
Create Date: 2020-07-27 00:34:01.263585

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = '81eebef26aba'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'sites',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String),
        sa.Column('url', sa.Text),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('sites')

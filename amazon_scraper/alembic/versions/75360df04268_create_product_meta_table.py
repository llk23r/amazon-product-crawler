"""Create product_meta table

Revision ID: 75360df04268
Revises: c6862a2c92bb
Create Date: 2020-07-27 01:24:28.466542

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = '75360df04268'
down_revision = 'c6862a2c92bb'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'product_meta',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('metadata_id', sa.Integer, sa.ForeignKey('metadata.id')),
        sa.Column('product_id', UUID(as_uuid=True), sa.ForeignKey('products.id')),

        sa.Column('active', sa.Boolean, nullable=False),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('product_meta')

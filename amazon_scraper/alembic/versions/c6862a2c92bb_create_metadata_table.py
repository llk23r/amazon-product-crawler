"""Create metadata table

Revision ID: c6862a2c92bb
Revises: 8174a432ae7b
Create Date: 2020-07-27 01:23:50.934254

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c6862a2c92bb'
down_revision = '8174a432ae7b'
branch_labels = None
depends_on = None

def upgrade():
    op.create_table(
        'metadata',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String),
        sa.Column('value', sa.String),
        sa.Column('site_id', sa.Integer, sa.ForeignKey('sites.id')),
        sa.Column('archived_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime,
                  onupdate=sa.func.now(), default=sa.func.now()),
        sa.Column('created_at', sa.DateTime, default=sa.func.now())
    )


def downgrade():
    op.drop_table('metadata')

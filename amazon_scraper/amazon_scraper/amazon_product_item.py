import scrapy

class AmazonProductItem(scrapy.Item):
    product_name = scrapy.Field()
    product_rating = scrapy.Field()
    product_features = scrapy.Field()
    product_sale_price = scrapy.Field()
    product_category = scrapy.Field()
    product_original_price = scrapy.Field()
    product_availability = scrapy.Field()
    product_description = scrapy.Field()

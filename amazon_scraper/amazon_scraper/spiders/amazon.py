# -*- coding: utf-8 -*-

import scrapy
from scrapy import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from itertools import zip_longest
from amazon_scraper.amazon_product_item import AmazonProductItem
from amazon_scraper.amazon_review_item import AmazonReviewItem
from amazon_scraper.items import AmazonScraperItem
import logging


class AmazonProductSpider(scrapy.Spider):
    name = "amazon_product_spider"
    allowed_domains = ["amazon.in", "amazon.com"]
    start_urls = [
        "https://www.amazon.in/Apple-MacBook-16-inch-Storage-Intel-Core-i7/dp/B081JWZQJB"]
    # start_urls = [
    # "https://www.amazon.in/Wonderchef-63153102-Nutri-Pot-3L/dp/B07ZRZTDJY"]

    myBaseUrl = ''
    start_urls = []

    def __init__(self, category='', **kwargs):
        self.myBaseUrl = category
        self.start_urls.append(self.myBaseUrl)
        self.depth_count = 0
        self.depth_limit = 3
        logger = logging.getLogger()
        logger.setLevel(logging.WARNING)
        super().__init__(**kwargs)

    # This will tell scrapy to store the scraped data to outputfile.json and for how long the spider should run.
    custom_settings = {
        'FEED_URI': 'amazon_scraper/outputfile.json', 'CLOSESPIDER_TIMEOUT': 80}

    def parse(self, response):
        yield scrapy.Request(response.url, callback=self.parse_product)

    def parse_product(self, response):
        items = AmazonProductItem()
        formatted_attributes = AmazonScraperItem()
        title = response.xpath('//h1[@id="title"]/span/text()').extract()
        sale_price = response.xpath(
            '//span[contains(@id,"ourprice") or contains(@id,"saleprice")]/text()').extract()
        category = response.xpath(
            '//a[@class="a-link-normal a-color-tertiary"]/text()').extract()
        availability = response.xpath(
            '//div[@id="availability"]//text()').extract()
        items['product_name'] = ''.join(title).strip()
        items['product_description'] = response.xpath(
            '//*[@id="productDescription"]/p/text()').extract_first()
        items['product_features'] = ' '.join(response.css('div.a-spacing-medium.a-spacing-top-small').css(
            '#feature-bullets').css('li>span::text').extract()).replace('\n', '')
        if sale_price:
            items['product_sale_price'] = float(
                ''.join(sale_price).strip()[2:].replace(',', ''))
        items['product_availability'] = ''.join(availability).strip()
        items['product_rating'] = response.css(
            '.reviewCountTextLinkedHistogram i.a-icon-star').xpath('span/text()').extract_first()[:3]
        formatted_attributes['product'] = dict(items)
        formatted_attributes['reviews'] = []
        formatted_attributes['categories'] = ','.join(
            map(lambda x: x.strip(), category)).strip()
        all_reviews = response.xpath(
            '//div[@data-hook="reviews-medley-footer"]//a[@data-hook="see-all-reviews-link-foot"]/@href').extract_first()
        reviews_url = response.follow(
            "https://www.amazon.in" + all_reviews).url
        self.logger.warning(f"\n\n1.PRODUCT Reviews URL: -->{reviews_url}")
        self.logger.warning(
            f"\n\n2.Formatted Attributes: -->{formatted_attributes}")
        yield scrapy.Request(reviews_url, callback=self.parse_reviews_page, meta={'product_details': formatted_attributes})

    def parse_reviews_page(self, response):
        product_details = response.meta.get('product_details')
        names = response.xpath(
            '//div[@data-hook="review"]//span[@class="a-profile-name"]/text()').extract()
        reviewerLink = response.xpath(
            '//div[@data-hook="review"]//a[@class="a-profile"]/@href').extract()
        reviewTitles = response.xpath(
            '//a[@data-hook="review-title"]/span/text()').extract()
        reviewBody = (response.xpath(
            '//span[@data-hook="review-body"]/span').xpath("normalize-space()").getall())
        verifiedPurchase = response.xpath(
            '//span[@data-hook="avp-badge"]/text()').extract()
        postDate = response.xpath(
            '//span[@data-hook="review-date"]/text()').extract()
        starRating = response.xpath(
            '//i[@data-hook="review-star-rating"]/span[@class="a-icon-alt"]/text()').extract()
        helpful = response.xpath(
            '//span[@class="cr-vote"]//span[@data-hook="helpful-vote-statement"]/text()').extract()

        for (name, reviewLink, title, Review, Verified, date, rating, helpful_count) in zip_longest(names, reviewerLink, reviewTitles, reviewBody, verifiedPurchase, postDate, starRating, helpful):
            next_urls = response.css(".a-last > a::attr(href)").extract_first()
            product_details['reviews'].append(
                dict(AmazonReviewItem(
                    name=name,
                    reviewerLink=reviewLink,
                    reviewTitle=title,
                    reviewBody=Review,
                    verifiedPurchase=Verified,
                    postDate=date,
                    starRating=rating,
                    helpful=helpful_count,
                    nextPage=next_urls)
                ))
        next_page = response.css('.a-last > a::attr(href)').extract_first()
        if next_page and self.depth_count < self.depth_limit:
            self.depth_count += 1
            redirect_url = response.follow(
                "https://www.amazon.in" + next_page).url
            self.logger.warning(f"\n\n4.NEXT PAGE: -->{redirect_url}")
            yield scrapy.Request(redirect_url, callback=self.parse_reviews_page, meta={'product_details': product_details})
        else:
            self.logger.warning(f"\n\n5.PRODUCT DETAILS: -->{product_details}")
            yield product_details

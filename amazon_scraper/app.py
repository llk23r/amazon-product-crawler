from flask import Flask, render_template, jsonify, request, redirect, url_for
from amazon_scraper.spiders.amazon import AmazonProductSpider
import time
from scrapy.signalmanager import dispatcher
from scrapy.crawler import CrawlerRunner
from scrapy import signals
import crochet
import os
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, subqueryload
import projectconfig
from models.sites import Site
from models.categories import Category
from models.products import Product
from models.metadata import Metadata
from models.reviews import Review
from models.product_meta import ProductMeta
from dateutil.parser import parse
from services.response_builder_service import ResponseBuilder
from services.model_resolver_service import ModelResolverService

db_engine = create_engine(projectconfig.db_url, pool_size=80, max_overflow=0)
Session = sessionmaker(bind=db_engine)
session = Session()

crochet.setup()
app = Flask(__name__)

output_data = []
crawl_runner = CrawlerRunner()


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/', methods=['POST'])
def submit():
    if request.method == 'POST':
        s = request.form['url']
        global baseURL
        baseURL = s
        if os.path.exists("amazon_scraper/outputfile.json"):
            os.remove("amazon_scraper/outputfile.json")

        return redirect(url_for('scrape'))


@app.route('/crawl', methods=['POST'])
def crawl():
    if request.method == 'POST':
        request_data = json.loads(request.data)
        source_url = request_data.get('url')
        scrape_with_crochet(source_url)
        time.sleep(35)
        jsonified_result = jsonify(output_data)
        return jsonified_result


@app.route("/scrape")
def scrape():
    scrape_with_crochet(baseURL=baseURL)
    time.sleep(35)
    return jsonify(output_data)


@crochet.run_in_reactor
def scrape_with_crochet(baseURL):
    dispatcher.connect(_crawler_result, signal=signals.item_scraped)
    eventual = crawl_runner.crawl(AmazonProductSpider, category=baseURL)
    return eventual

def _crawler_result(item, response, spider):
    model_resolver = ModelResolverService(item, response)
    model_resolver.create_and_resolve_site()
    model_resolver.create_and_resolve_products()
    model_resolver.create_product_wise_reviews()
    model_resolver.create_and_resolve_meta()
    response_builder_sevice = ResponseBuilder(dict(item))
    result = response_builder_sevice.format_api_response()
    output_data.append(result)


if __name__ == "__main__":
    app.run(debug=True)
